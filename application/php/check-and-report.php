<?php

/********************* CONFIGURATION ************************/

define('BACKUPS_FULL_PATH', '/path/to/backups/');
define('MAIL_RECIPIENTS', 'daniele.megna@nyxsolutions.it, support@officinaweb.pro');

// tarnameprefix => expected_min_size (MB)
global $backups_to_check;
$backups_to_check = array(
	"horeca_horeca_export_it" => 325,
	"eurotank_rainbowkayaks_com" => 85,
	"eurotank_dragorossi_it" => 60,
	"alpinaction" => 645,
	"noli" => 85,
	"canoashop" => 245,
	"old-canoashop" => 880,
	"aquadolcenet" => 85
);

global $full_backup_exp_size;
$full_backup_exp_size = array_sum(array_values($backups_to_check));
	
/********************* END CONFIGURATION ************************/

/********************* MAIN SCRIPT ************************/

$status = getStatusMessage(BACKUPS_FULL_PATH, $backups_to_check);
echo "Notes: ".$status.PHP_EOL;
$mailMessage = buildReportEmail($status, getTodayQuota(), getLsContent());
sendMail($mailMessage);

/********************* END MAIN SCRIPT ************************/

/********************* FUNCTIONS ************************/

function getStatusMessage($path, $backups_to_check)
{
	$message = '';

	$message .= getStatusMessageForLastFullMerge($path);
	$message .= getStatusMessageForTonightBackups($path, $backups_to_check);

	return $message;
}

function compareBigIntegersWrappedIntoStrings($n1, $n2)
{
	if(strlen($n1) != strlen($n2))
		return ((strlen($n1) > strlen($n2)) ? 1 : -1);

	$lenght = strlen($n1);
	while($i < $lenght){
		if($n1[$i] != $n2[$i])
			return (($n1[$i] > $n2[$i]) ? 1 : -1);

		$i++;
	}

	return 0;
}

function getFilesizeIntString($filepath)
{
	return exec("stat -c%s '$filepath'");
}

function getStatusMessageForLastFullMerge($path)
{
	$date = date('Y-m-d', strtotime('-2 days'));
	$day = date('d', strtotime($date));
	$month = date('m', strtotime($date));
	$year = date('Y', strtotime($date));

	$full_backup_name = "all-$year$month$day.tar";
	if(!file_exists($path.$full_backup_name)) {
		return "Cannot find '$full_backup_name'!<br/>'";
	}

	/*$filepath = $path.$full_backup_name;
	if(!isFilesizeAlmostMiniumValue($filepath, $full_backup_exp_size)) {
		return "Unexpected size for full backup '$full_backup_name' (expected almost $full_backup_exp_size MB)<br/>";;
	}*/

	return '';
}

function isFilesizeAlmostMiniumValue($filepath, $min)
{
	$size = getFilesizeIntString($filepath);
	$min = strval($min*1048576); // convert MB in Bytes
	$comparison = compareBigIntegersWrappedIntoStrings($size, $min);

	return ($comparison != -1);
}

function getStatusMessageForTonightBackups($path, $backups_to_check)
{
	$message = '';

	$day = date('d', strtotime(date()));
	$month = date('m', strtotime(date()));
	$year = date('Y', strtotime(date()));

	foreach(array_keys($backups_to_check) as $check) {
		$pattern = $check."_$year$month$day*.tar";
		$glob = glob($path.$pattern);

		if(count($glob) == 0) {
			$message .= "Cannot find tonight $pattern !<br/>";	
			continue;
		}

		$filepath = $glob[0];
		$exp_size = $backups_to_check[$check];
		if(!isFilesizeAlmostMiniumValue($filepath, $exp_size)) {
			$message .= "Unexpected size for $pattern (expected almost $exp_size MB)<br/>";
			continue;
		}
	}

	return $message;
}

function getTodayQuota()
{
	$result = '';

	exec("quota -s 2> /dev/null", $output);
	foreach($output as $row)
		$result .= $row.'<br />';

	return $result;
}

function getLsContent($path = BACKUPS_FULL_PATH)
{
	$result = '<table id="dirlisten">';

	$row = exec("ls -lht $path | grep '.tar' | awk '{OFS=\"</td><td>\"; print $5, $6, $7, $9}'", $output, $error);
	while(list(,$row) = each($output)){
		$result .= "<tr><td>$row</td></tr>";
	}

	return $result.'</table>';
}

function sendMail($mailMessage)
{
	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
	$headers .= 'From: Rude Backup <rudebackup@ux01srv.officinaweb.pro>' . "\r\n";

	mail(MAIL_RECIPIENTS, 'Rude backup report', $mailMessage, $headers);
}

function buildReportEmail($statusMsg, $todayQuota, $dirListening)
{
	$template = '<html>
		<head>
			<style language="text/css">
				table#main { background-color:#EEEEEE; }	
				table#main th, table#main td { padding:8px 10px; }
				table#dirlisten td { padding:1px 6px; }
				table { border-collapse:collapse; }
				table td, table th { vertical-align:top; border:1px solid #DBDBDB; }
			</style>
		</head>
		<body>
			<table id="main">
				<tr style="background-color:%s">
					<th colspan="2">%s</th>
				</tr>
				<tr>
					<td>Today quota:</td>
					<td>%s</td>
				</tr>
				<tr>
					<td>Notes:</td>
					<td>%s</td>
				</tr>
				<tr>
					<td>Directory listening:</td>
					<td>%s</td>
				<tr>
			</table>
		</body>
	</html>';

	$statusOk = ($statusMsg == '');
	$statusColor = $statusOk ? '#CCFFCC' : '#FFCCCC';
	$message = 'Rude backup is ' . ($statusOk ? 'fine today :)' : 'not happy today :(');

	return sprintf($template, $statusColor, $message, $todayQuota, $statusMsg, $dirListening);
}

/********************* END FUNCTIONS ************************/
