#!/bin/bash

FINAL_TARNAME='sitename_'$(date +"%Y%m%d%H%M%S")'.tar'
TMP_FOLDERPATH='/tmp/'

FS_DESTINATION_FOLDER='/backups/'

# ======= hello
echo ' '
echo '========================= '$FINAL_TARNAME' '$(date)

# ======== fs backup targz
echo "fs tar.gz"
step_ok=false
attempts=10
while [ $step_ok = false ] && [ $attempts -gt 0 ]; do
  echo 'Attempts '$attempts' ..'
  let attempts-=1
  rm ${TMP_FOLDERPATH}fs.tar.gz -f
  #(tar -czf ${TMP_FOLDERPATH}fs.tar.gz --exclude=rudebackup_fsonly.sh -C $(dirname $0) .) && step_ok=true
  (tar -czf ${TMP_FOLDERPATH}fs.tar.gz -C $(dirname $0) .) && step_ok=true
done
if [ $step_ok = false ]; then
  echo 'Operation aborted for attempts'
  exit 0
fi

# ======== final tar
echo "final full tar"
step_ok=false
attempts=10
while [ $step_ok = false ] && [ $attempts -gt 0 ]; do
  echo 'Attempts '$attempts' ..'
  let attempts-=1
  rm ${TMP_FOLDERPATH}full.tar -f
  (tar -cf ${TMP_FOLDERPATH}full.tar -C ${TMP_FOLDERPATH} fs.tar.gz) && step_ok=true
done
if [ $step_ok = false ]; then
  echo 'Operation aborted for attempts'
  exit 0
fi

# ======== rm targzs
rm ${TMP_FOLDERPATH}fs.tar.gz

# ======== mv final tar
if [ -n "$FS_DESTINATION_FOLDER" ]; then
  echo "moving final full tar"
  mkdir -p $FS_DESTINATION_FOLDER
  mv ${TMP_FOLDERPATH}full.tar $FS_DESTINATION_FOLDER$FINAL_TARNAME
  exit 1
fi

# ======== rm final tar
rm ${TMP_FOLDERPATH}full.tar
