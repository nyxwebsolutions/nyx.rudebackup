#!/bin/bash

DB_HOST='localhost'
DB_USERNAME='';
DB_PASSWORD='';

DB_NEWDBNAME='';
DB_NEWUSERNAME='';
DB_NEWPASSWORD='';

BACKUP_FILENAME='./*.tar'

echo 'extracting full'
tar -xf $BACKUP_FILENAME 

echo 'extracting database'
tar -xf db.tar.gz

echo 'extracting filesystem'
tar -xf fs.tar.gz

#echo 'deleting extracted archives'
#rm $BACKUP_FILENAME 
#rm db.tar.gz
#rm fs.tar.gz

echo 'mysql drop db, create db and create user'
mysql -h $DB_HOST -u $DB_USERNAME -p$DB_PASSWORD -e "DROP DATABASE IF EXISTS $DB_NEWDBNAME;"
mysql -h $DB_HOST -u $DB_USERNAME -p$DB_PASSWORD -e "CREATE DATABASE IF NOT EXISTS $DB_NEWDBNAME;"
mysql -h $DB_HOST -u $DB_USERNAME -p$DB_PASSWORD -e "GRANT ALL ON $DB_NEWDBNAME.* TO '$DB_NEWUSERNAME'@'$DB_HOST' IDENTIFIED BY '$DB_NEWPASSWORD';"

echo 'mysql restore db'
mysql -h $DB_HOST -u $DB_NEWUSERNAME -p$DB_NEWPASSWORD $DB_NEWDBNAME < db.sql

rm db.sql

# most of our rudebackups are prestashops.... try to adjust db string connection
PRESTASHOP_CONFIG_FILEPATH='./config/settings.inc.php';
if [ -f $PRESTASHOP_CONFIG_FILEPATH ]
then
  echo "Found prestashop settings file: trying to fix connection string..."
  sed -ie "s/'_DB_SERVER_', '.*'/'_DB_SERVER_', '$DB_HOST'/g" $PRESTASHOP_CONFIG_FILEPATH
  sed -ie "s/'_DB_NAME_', '.*'/'_DB_NAME_', '$DB_NEWDBNAME'/g" $PRESTASHOP_CONFIG_FILEPATH
  sed -ie "s/'_DB_USER_', '.*'/'_DB_USER_', '$DB_NEWUSERNAME'/g" $PRESTASHOP_CONFIG_FILEPATH
  sed -ie "s/'_DB_PASSWD_', '.*'/'_DB_PASSWD_', '$DB_PASSWORD'/g" $PRESTASHOP_CONFIG_FILEPATH
fi
